component {
    
    function up( schema ) {
        schema.create( "users", function( table ) {
            table.increments( "id" );
            table.string( "username" ).unique();
            table.string( "email" ).unique();
            table.string( "password" );
            table.tinyInteger( "active" );
            table.timestamp( "createdDate" );
            table.timestamp( "updatedDate" );
        } );    
    }

    function down( schema ) {
        
    }

}
