/**
 * A ColdBox Event Handler
 */
component{
	property name="wirebox" inject="wirebox";

	/**
	 * Home page
	 */
	function index( event, rc, prc ){
		// writeDump(wirebox.getBinder().getMappings().keyArray())
		prc.users = getInstance( "User@play1" )
			.where( "active", 1 )
			.orderBy( "username", "desc" )
			.limit( 10 )
			.get();
			writeDump(prc.users.get())
		for ( var user in prc.users.get() ) {
			writeOutput( user.getUsername() );
		}
		event.setView( "home/index" );
	}

	    // /users/:id
    function show( event, rc, prc ) {
        // this finds the User with an id of 1 and retrieves it
		prc.user = getInstance( "User@play1" ).findOrFail( rc.id );
		writeDump(prc.user)
		writeDump(prc.user.getusername())
        event.setView( "home/show" );
	}
	
	function seed( event, rc, prc) {
		var user = getInstance( "User@play1" );
		user.setUsername( "JaneDoe" );
		user.setEmail( "jane@example.com" );
		user.setPassword( "mypass1234" );
		user.setActive( 1 );
		user.save();
	}

}
